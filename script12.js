"use strict"
let listBtn = document.querySelectorAll(".btn");
window.addEventListener("keyup", (e) => {
  console.log(e);
  let getlistBtn = document.querySelector(`.btn-${e.code.toLowerCase()}`);

  if (getlistBtn) {
    getlistBtn.classList.add("btnBlue");
  }
  for (let button of listBtn) {
    if (button !== getlistBtn) {
        button.classList.remove("btnBlue");
    }
    
  }
});
